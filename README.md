# Keith
_I didn't name it, don't ask me_

This is the code for Jungle Robotics 3627's robot for the 2023 FRC
Charged Up season.

## User Guide
After turning on the robot, you are to leave it to rest for at least 15 seconds for the gyro to properly calibrate.

Requires 2 drivers, one for the chassis, one for the crane

Chassis driver uses an Xbox controller in the 0th slot of the driver station joystick menu. Crane driver uses a Logitech
flight stick in the 1st slot of the driver station joystick menu.

#### Chassis Controls
_Note the Mecanum chassis allows for decoupling rotation from direction of travel_

- Left joystick controls direction of travel
- Right joystick controls rotation of robot
- Pressing `Y` resets the yaw to where the chassis is currently facing
- `Angle Offset` in SmartDashboard is the offset that determines where forward is. For example, the default offset of
  `180` sets the forward direction of travel to be `180` degrees away from the yaw of `0`, which is originally the 
  facing when powered on, or the facing when `Y` was last pressed

#### Crane Controls

#### Auto Selections

### Project Roadmap
- [X] Vision
  - [X] AprilTag detection
  - [X] Determine field relative position using AprilTag data
- [X] Driving
  - [X] Manual driving
    - [X] Raw velocity driving
    - [X] Field relative driving
- [X] Game Piece Manipulation
  - [X] Crane lifts game piece to required heights
  - [X] Claw grasps game pieces
- [X] Input Controls and Dashboard
  - [X] Crane height buttons: stow, low, mid, high
  - [X] Claw buttons: open, closeCube, closeCone
  - [X] Drive controls: joysticks on xbox
  - [X] Field map on dashboard
  - [X] Autonomous settings from dashboard
- [X] Autonomous
  - [X] Auto pick up
  - [X] Auto score
  - [X] Auto leave community
- [ ] Documentation
  - [ ] Constants clearly defined
  - [ ] Provide shuffleboard layouts
  - [ ] Clean up and explain code
  - [ ] User guide