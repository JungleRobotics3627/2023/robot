package frc.junglerobotics3627.keith.command

import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard
import edu.wpi.first.wpilibj2.command.CommandBase
import frc.junglerobotics3627.keith.subsystem.DrivetrainSubsystem

class TuneDriveToCommand : CommandBase() {
    init {
        SmartDashboard.putNumber("PPMecanumController kP", 0.0)
        SmartDashboard.putNumber("PPMecanumController kI", 0.0)
        SmartDashboard.putNumber("PPMecanumController kD", 0.0)

        addRequirements(DrivetrainSubsystem)
    }
}