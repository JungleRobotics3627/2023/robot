package frc.junglerobotics3627.keith.command

import edu.wpi.first.wpilibj2.command.CommandBase
import frc.junglerobotics3627.keith.subsystem.DrivetrainSubsystem
import frc.junglerobotics3627.keith.ui.InputSystem

class TeleopDriveCommand : CommandBase() {
    init {
        addRequirements(DrivetrainSubsystem)
    }

    override fun execute() {
        // DrivetrainSubsystem.velocityDrive(InputSubsystem.driveX, InputSubsystem.driveY, InputSubsystem.driveZ)
        DrivetrainSubsystem.fieldRelativeDrive(InputSystem.driveX, InputSystem.driveY, InputSystem.driveZ)
        // InputSystem.rumble(DrivetrainSubsystem.jerk)
    }
}