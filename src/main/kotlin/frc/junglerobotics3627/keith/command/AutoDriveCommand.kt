package frc.junglerobotics3627.keith.command

import edu.wpi.first.wpilibj2.command.CommandBase
import frc.junglerobotics3627.keith.subsystem.DrivetrainSubsystem

class AutoDriveCommand : CommandBase() {
    init {
        addRequirements(DrivetrainSubsystem)
    }
}