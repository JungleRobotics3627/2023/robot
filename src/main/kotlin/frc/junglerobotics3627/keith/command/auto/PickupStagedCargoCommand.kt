package frc.junglerobotics3627.keith.command.auto

import edu.wpi.first.math.geometry.Pose2d
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup
import frc.junglerobotics3627.keith.command.crane.ClawCommand
import frc.junglerobotics3627.keith.command.crane.DeployCommand
import frc.junglerobotics3627.keith.command.crane.UpperCommand
import frc.junglerobotics3627.keith.subsystem.CraneSubsystem
import frc.junglerobotics3627.keith.subsystem.DrivetrainSubsystem

class PickupStagedCargoCommand(stagedCargoLocation: Pose2d) : SequentialCommandGroup() {
    init {
        addCommands(
            DeployCommand().andThen(UpperCommand(CraneSubsystem.UpperCraneState.Low)),
            ClawCommand(CraneSubsystem.ClawState.Open).get,
            DrivetrainSubsystem.getDriveToCommand(stagedCargoLocation),
            ClawCommand(CraneSubsystem.ClawState.CloseCube).get,
        )
    }
}