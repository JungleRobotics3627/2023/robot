package frc.junglerobotics3627.keith.command.auto

import edu.wpi.first.wpilibj2.command.CommandBase
import frc.junglerobotics3627.keith.Constants.Drivetrain.maxVelocity
import frc.junglerobotics3627.keith.subsystem.DrivetrainSubsystem
import frc.junglerobotics3627.keith.subsystem.DrivetrainSubsystem.pitch
import kotlin.math.pow

/**
 * Autonomously balances the robot on the charge station
 * Assumes the robot is aligned in front of the charge station
 */
class BalanceCommand : CommandBase() {

    private val boardedAngle = 5.0 // degrees
    private var boarded = false
    private val weight = 0.5


    init {
        addRequirements(DrivetrainSubsystem)
    }

    override fun initialize() {
    }

    override fun execute() {
        if (!boarded) {
            board()
        } else {
            balance()
        }
    }

    private fun board() {
        DrivetrainSubsystem.rawVelocityDrive(maxVelocity, 0.0, 0.0)
        if (pitch > boardedAngle) {
            boarded = true
        }
    }

    private fun balance() {
        // drive at a percentage of the max velocity, based on the pitch (with an x^2 curve)
        DrivetrainSubsystem.rawVelocityDrive(0.5 * maxVelocity * (pitch / 15).pow(2), 0.0, 0.0)
    }

    override fun end(interrupted: Boolean) {
        DrivetrainSubsystem.stopMotors()
    }

    override fun isFinished() = boarded && pitch in -3.0..3.0
}