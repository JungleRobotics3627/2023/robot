package frc.junglerobotics3627.keith.command.auto

import edu.wpi.first.math.geometry.Pose2d
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup
import frc.junglerobotics3627.keith.command.crane.ClawCommand
import frc.junglerobotics3627.keith.command.crane.DeployCommand
import frc.junglerobotics3627.keith.command.crane.UpperCommand
import frc.junglerobotics3627.keith.subsystem.CraneSubsystem
import frc.junglerobotics3627.keith.subsystem.DrivetrainSubsystem

class ScoreGamePieceCommand(nodeScoringLocation: Pose2d) : SequentialCommandGroup() {
    init {
        addCommands(
            DeployCommand().andThen(UpperCommand(CraneSubsystem.UpperCraneState.High)),
            DrivetrainSubsystem.getDriveToCommand(nodeScoringLocation),
            ClawCommand(CraneSubsystem.ClawState.Open).get
        )
    }
}