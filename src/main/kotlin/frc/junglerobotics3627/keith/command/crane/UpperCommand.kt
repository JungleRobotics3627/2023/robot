package frc.junglerobotics3627.keith.command.crane

import edu.wpi.first.wpilibj2.command.CommandBase
import frc.junglerobotics3627.keith.subsystem.CraneSubsystem
import frc.junglerobotics3627.keith.subsystem.CraneSubsystem.UpperCraneState
import frc.junglerobotics3627.keith.subsystem.CraneSubsystem.checkUpperCranePosition
import frc.junglerobotics3627.keith.subsystem.CraneSubsystem.setUpperCraneSetpoint

class UpperCommand(private val upperCraneState: UpperCraneState) : CommandBase() {
    init {
        addRequirements(CraneSubsystem)
    }

    override fun execute() {
        setUpperCraneSetpoint(upperCraneState)
    }

    override fun isFinished() = checkUpperCranePosition(upperCraneState)
}