package frc.junglerobotics3627.keith.command.crane

import edu.wpi.first.wpilibj2.command.CommandBase
import frc.junglerobotics3627.keith.subsystem.CraneSubsystem

/**
 * Unsafe, manual control that deploys the lower crane
 */
class ManualDeployCommand : CommandBase() {
    init {
        addRequirements(CraneSubsystem)
    }

    override fun initialize() {
        CraneSubsystem.setLowerCrane(CraneSubsystem.LowerCraneState.Deploy)
    }

    override fun isFinished(): Boolean {
        return CraneSubsystem.checkLowerCranePosition(CraneSubsystem.LowerCraneState.Deploy)
    }

    override fun end(interrupted: Boolean) {
        CraneSubsystem.setLowerCrane(CraneSubsystem.LowerCraneState.Rest)
    }
}