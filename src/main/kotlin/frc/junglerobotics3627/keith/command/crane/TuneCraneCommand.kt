package frc.junglerobotics3627.keith.command.crane
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard
import edu.wpi.first.wpilibj2.command.CommandBase
import frc.junglerobotics3627.keith.Constants
import frc.junglerobotics3627.keith.subsystem.CraneSubsystem
import kotlin.math.abs
import kotlin.math.sin

class TuneCraneCommand : CommandBase() {
    init {
        SmartDashboard.putNumber("upper.p", Constants.Crane.Upper.PID.p)
        SmartDashboard.putNumber("upper.i", Constants.Crane.Upper.PID.i)
        SmartDashboard.putNumber("upper.d", Constants.Crane.Upper.PID.d)
        SmartDashboard.putNumber("upper.f", Constants.Crane.Upper.PID.fSinFactor)
        SmartDashboard.putNumber("upper.conversion_factor", Constants.Crane.Upper.PID.rotationsToRadians)
        SmartDashboard.putNumber("upper.offset", Constants.Crane.Upper.PID.offset)
        SmartDashboard.putNumber("upper.position", 0.0)
        SmartDashboard.putBoolean("upper.run", false)
        SmartDashboard.putNumber("lower.speed", 0.0)
        SmartDashboard.putBoolean("lower.run", false)
        SmartDashboard.putNumber("claw.speed", 0.0)
        SmartDashboard.putBoolean("claw.run", false)

        addRequirements(CraneSubsystem)
    }

    private var dashboardValues: Map<String, Double> = mapOf()
    private var upperRun = false
    private var lowerRun = false
    private var clawRun = false
    override fun initialize() {
        CraneSubsystem.isTuningMode = true
    }

    override fun execute() {
        SmartDashboard.putNumber("upper.process_variable", CraneSubsystem.tuningCraneProcessVariable)
        SmartDashboard.putNumber("lower.process_variable", CraneSubsystem.tuningLowerProcessVariable)
        SmartDashboard.putNumber("claw.process_variable", CraneSubsystem.tuningClawProcessVariable)

        updateValues()
        updateFF()
        run()
    }

    override fun end(interrupted: Boolean) {
        CraneSubsystem.isTuningMode = false
    }

    private fun updateFF() {
        // dashboardValues["kS"]
        // setpoint velocity and acceleration are used here

        val p = dashboardValues["upper.p"]!!
        val i = dashboardValues["upper.i"]!!
        val d = dashboardValues["upper.d"]!!
        val f = dashboardValues["upper.f"]!!
        val factor = dashboardValues["upper.conversion_factor"]!!
        val offset = dashboardValues["upper.offset"]!!
        //CraneSubsystem.tuneSetPIDF(p, i, d, f)
        CraneSubsystem.tuneSetPIDF(p, i, d, f * abs(sin((CraneSubsystem.tuningCraneProcessVariable + offset) * factor)))
    }


    private fun updateValues() {
        dashboardValues = mapOf(
            "upper.p" to SmartDashboard.getNumber("upper.p", 0.0),
            "upper.i" to SmartDashboard.getNumber("upper.i", 0.0),
            "upper.d" to SmartDashboard.getNumber("upper.d", 0.0),
            "upper.f" to SmartDashboard.getNumber("upper.f", 0.0),
            "upper.conversion_factor" to SmartDashboard.getNumber("upper.conversion_factor", 0.0),
            "upper.offset" to SmartDashboard.getNumber("upper.offset", 0.0),
            "upper.position" to SmartDashboard.getNumber("upper.position", 0.0),
            "lower.speed" to SmartDashboard.getNumber("lower.speed", 0.0),
            "claw.speed" to SmartDashboard.getNumber("claw.speed", 0.0),
        )
        upperRun = SmartDashboard.getBoolean("upper.run", false)
        lowerRun = SmartDashboard.getBoolean("lower.run", false)
        clawRun = SmartDashboard.getBoolean("claw.run", false)
    }

    private fun run() {
        if (upperRun) {
            CraneSubsystem.isRunningTuning = true
            CraneSubsystem.tuningCraneSetpoint = dashboardValues["upper.position"]!!
        } else {
            CraneSubsystem.isRunningTuning = false
        }

        if (lowerRun) {
            CraneSubsystem.tuningLowerSetpoint = dashboardValues["lower.speed"]!!
        } else {
            CraneSubsystem.tuningLowerSetpoint = 0.0
        }

        if (clawRun) {
            CraneSubsystem.tuningClawSetpoint = dashboardValues["claw.speed"]!!
        } else {
            CraneSubsystem.tuningClawSetpoint = 0.0
        }
    }
}