package frc.junglerobotics3627.keith.command.crane

import edu.wpi.first.wpilibj2.command.CommandBase
import frc.junglerobotics3627.keith.subsystem.CraneSubsystem
import frc.junglerobotics3627.keith.subsystem.CraneSubsystem.setLowerCrane

class LowerCommand(private val lowerCraneState: CraneSubsystem.LowerCraneState) : CommandBase() {
    init {
        addRequirements(CraneSubsystem)
    }

    override fun execute() {
        setLowerCrane(lowerCraneState)
    }

    override fun isFinished() = CraneSubsystem.checkLowerCranePosition(lowerCraneState)
}