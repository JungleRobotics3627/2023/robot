package frc.junglerobotics3627.keith.command.crane

import edu.wpi.first.wpilibj2.command.CommandBase
import frc.junglerobotics3627.keith.subsystem.CraneSubsystem
import frc.junglerobotics3627.keith.subsystem.CraneSubsystem.ClawState
import frc.junglerobotics3627.keith.subsystem.CraneSubsystem.LowerCraneState
import frc.junglerobotics3627.keith.subsystem.CraneSubsystem.UpperCraneState
import frc.junglerobotics3627.keith.subsystem.CraneSubsystem.checkLowerCranePosition
import frc.junglerobotics3627.keith.subsystem.CraneSubsystem.checkUpperCranePosition
import frc.junglerobotics3627.keith.subsystem.CraneSubsystem.isClawSafeForLowerCraneMotion
import frc.junglerobotics3627.keith.subsystem.CraneSubsystem.setClaw
import frc.junglerobotics3627.keith.subsystem.CraneSubsystem.setLowerCrane
import frc.junglerobotics3627.keith.subsystem.CraneSubsystem.setUpperCraneSetpoint

/**
 * This command retracts both cranes and disables the claw, with checks on the claw and upper crane to ensure safety
 */
class StowCommand : CommandBase() {
    init {
        addRequirements(CraneSubsystem)
    }

    private var safeToStow = checkUpperCranePosition(UpperCraneState.Safe) && isClawSafeForLowerCraneMotion

    override fun initialize() {
        println("StowCommand#initialize" +
                "|checkLowerCranePosition(LowerCraneState.Stow): ${checkLowerCranePosition(LowerCraneState.Stow)}")
    }

    override fun execute() {
        println("StowCommand#execute" +
                "|isFinished: $isFinished" +
                "|safeToStow: $safeToStow")
        // if (isFinished) return

        if (safeToStow) stow() else makeSafe()
    }

    override fun end(interrupted: Boolean) {
        println("StowCommand#end")
        setLowerCrane(LowerCraneState.Rest)
        setClaw(ClawState.Rest)
    }

    override fun isFinished() = checkLowerCranePosition(LowerCraneState.Stow)
            && checkUpperCranePosition(UpperCraneState.Stow)
            && isClawSafeForLowerCraneMotion

    private fun stow() {
        println("StowCommand#stow")
        if (checkLowerCranePosition(LowerCraneState.Stow)) {
            if (checkUpperCranePosition(UpperCraneState.Stow)) {
            } else {
                setUpperCraneSetpoint(UpperCraneState.Stow)
            }
        } else {
            setLowerCrane(LowerCraneState.Stow)
            setUpperCraneSetpoint(UpperCraneState.Safe)
        }
    }

    private fun makeSafe() {
        println("StowCommand#makeSafe")
        setUpperCraneSetpoint(UpperCraneState.Safe)
        setClaw(ClawState.CloseCube)

        safeToStow = checkUpperCranePosition(UpperCraneState.Safe) && isClawSafeForLowerCraneMotion
    }
}