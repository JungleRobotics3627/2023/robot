package frc.junglerobotics3627.keith.command.crane

import edu.wpi.first.wpilibj2.command.Command
import edu.wpi.first.wpilibj2.command.Commands
import frc.junglerobotics3627.keith.subsystem.CraneSubsystem.ClawState
import frc.junglerobotics3627.keith.subsystem.CraneSubsystem.setClaw

/**
 * Simple command that sets the claw state
 */
class ClawCommand(private val clawState: ClawState) {
    val get: Command = Commands.runOnce({ setClaw(clawState) })
}