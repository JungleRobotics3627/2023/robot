package frc.junglerobotics3627.keith.command.crane

import edu.wpi.first.wpilibj2.command.CommandBase
import frc.junglerobotics3627.keith.subsystem.CraneSubsystem
import frc.junglerobotics3627.keith.subsystem.CraneSubsystem.ClawState
import frc.junglerobotics3627.keith.subsystem.CraneSubsystem.LowerCraneState
import frc.junglerobotics3627.keith.subsystem.CraneSubsystem.UpperCraneState
import frc.junglerobotics3627.keith.subsystem.CraneSubsystem.checkLowerCranePosition
import frc.junglerobotics3627.keith.subsystem.CraneSubsystem.checkUpperCranePosition
import frc.junglerobotics3627.keith.subsystem.CraneSubsystem.isClawSafeForLowerCraneMotion
import frc.junglerobotics3627.keith.subsystem.CraneSubsystem.setClaw
import frc.junglerobotics3627.keith.subsystem.CraneSubsystem.setLowerCrane
import frc.junglerobotics3627.keith.subsystem.CraneSubsystem.setUpperCraneSetpoint

class DeployCommand : CommandBase() {
    init {
        addRequirements(CraneSubsystem)
    }

    private var safeToDeploy = checkUpperCranePosition(UpperCraneState.Safe) && isClawSafeForLowerCraneMotion

    override fun execute() {
        if (isFinished) return // skip if we are finished

        // make it safe before deploying
        if (safeToDeploy) setLowerCrane(LowerCraneState.Deploy) else makeSafe()
    }

    override fun end(interrupted: Boolean) {
        setLowerCrane(LowerCraneState.Rest)
    }

    override fun isFinished() = checkLowerCranePosition(LowerCraneState.Deploy)

    private fun makeSafe() {
        if (!checkUpperCranePosition(UpperCraneState.Safe)) {
            setUpperCraneSetpoint(UpperCraneState.Safe)
        }

        if (!isClawSafeForLowerCraneMotion) {
            setClaw(ClawState.CloseCube)
        }

        safeToDeploy = checkUpperCranePosition(UpperCraneState.Safe) && isClawSafeForLowerCraneMotion
    }
}