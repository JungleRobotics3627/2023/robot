package frc.junglerobotics3627.keith.command.crane

import edu.wpi.first.wpilibj2.command.CommandBase
import frc.junglerobotics3627.keith.subsystem.CraneSubsystem

class AdjustUpperCommand(private val adjustment: Double) : CommandBase() {
    init {
        addRequirements(CraneSubsystem)
    }

    private var isFinished = false

    override fun execute() {
        CraneSubsystem.adjustUpperCraneSetpoint(adjustment)

        isFinished = true
    }

    override fun isFinished() = isFinished
}