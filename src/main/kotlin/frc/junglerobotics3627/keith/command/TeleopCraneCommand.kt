package frc.junglerobotics3627.keith.command

import edu.wpi.first.wpilibj2.command.CommandBase
import frc.junglerobotics3627.keith.subsystem.CraneSubsystem
import frc.junglerobotics3627.keith.ui.InputSystem
import kotlin.math.abs

class TeleopCraneCommand : CommandBase() {
    init {
        addRequirements(CraneSubsystem)
    }

    override fun execute() {
        if (abs(InputSystem.upperCraneSpeed) >= .1) {
            CraneSubsystem.setUpperCraneRaw(InputSystem.upperCraneSpeed)
        }

        if (InputSystem.deployLowerCrane) {
            CraneSubsystem.setLowerCrane(CraneSubsystem.LowerCraneState.Deploy)
        }

        if (InputSystem.stowLowerCrane) {
            CraneSubsystem.setLowerCrane(CraneSubsystem.LowerCraneState.Stow)
        }
    }
}