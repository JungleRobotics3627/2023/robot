package frc.junglerobotics3627.keith

import edu.wpi.first.wpilibj.TimedRobot
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard
import edu.wpi.first.wpilibj2.command.Command
import edu.wpi.first.wpilibj2.command.CommandScheduler
import edu.wpi.first.wpilibj2.command.Commands
import frc.junglerobotics3627.keith.command.TeleopCraneCommand
import frc.junglerobotics3627.keith.command.TeleopDriveCommand
import frc.junglerobotics3627.keith.command.crane.TuneCraneCommand
import frc.junglerobotics3627.keith.subsystem.CraneSubsystem
import frc.junglerobotics3627.keith.subsystem.DrivetrainSubsystem
import frc.junglerobotics3627.keith.ui.InputSystem

class Robot : TimedRobot() {
    private val teleopCraneCommand = TeleopCraneCommand()
    private val emptyCraneCommand = Commands.run({}, CraneSubsystem)
    private val emptyDriveCommand = Commands.run({}, DrivetrainSubsystem)

    private lateinit var autoCommand: Command

    override fun robotInit() {
        SmartDashboard.putBoolean("Just Taxi", false)
        SmartDashboard.putBoolean("Just Score", true)
        SmartDashboard.putBoolean("Score + Taxi", false)
        SmartDashboard.putNumber("Starting Angle", 180.0)

        CraneSubsystem.defaultCommand = emptyCraneCommand
        DrivetrainSubsystem.defaultCommand = emptyDriveCommand
        autoCommand = InputSystem.autoCommand
    }
    override fun robotPeriodic() {
        CommandScheduler.getInstance().run()
    }

    override fun disabledInit() {}
    override fun disabledPeriodic() {}
    override fun disabledExit() {}

    override fun autonomousInit() {
        CommandScheduler.getInstance().cancelAll()

        CraneSubsystem.defaultCommand = emptyCraneCommand
        DrivetrainSubsystem.defaultCommand = emptyDriveCommand
        autoCommand.schedule()
    }
    override fun autonomousPeriodic() {}
    override fun autonomousExit() {}

    override fun teleopInit() {
        CommandScheduler.getInstance().cancelAll()
        CraneSubsystem.defaultCommand = teleopCraneCommand
        DrivetrainSubsystem.defaultCommand = TeleopDriveCommand()

        // CraneSubsystem.defaultCommand = TuneCraneCommand()
    }
    override fun teleopPeriodic() {}
    override fun teleopExit() {}
}