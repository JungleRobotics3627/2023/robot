package frc.junglerobotics3627.keith.ui

import edu.wpi.first.math.MathUtil.clamp
import edu.wpi.first.math.geometry.Pose2d
import edu.wpi.first.math.geometry.Rotation2d
import edu.wpi.first.wpilibj.GenericHID
import edu.wpi.first.wpilibj.Joystick
import edu.wpi.first.wpilibj.XboxController
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard
import edu.wpi.first.wpilibj2.command.Command
import edu.wpi.first.wpilibj2.command.Commands
import edu.wpi.first.wpilibj2.command.RepeatCommand
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup
import edu.wpi.first.wpilibj2.command.button.Trigger
import frc.junglerobotics3627.keith.Constants
import frc.junglerobotics3627.keith.Constants.ButtonBoardPorts
import frc.junglerobotics3627.keith.Constants.Drivetrain.maxVelocity
import frc.junglerobotics3627.keith.command.auto.PickupStagedCargoCommand
import frc.junglerobotics3627.keith.command.auto.ScoreGamePieceCommand
import frc.junglerobotics3627.keith.command.crane.*
import frc.junglerobotics3627.keith.subsystem.CraneSubsystem
import frc.junglerobotics3627.keith.subsystem.CraneSubsystem.ClawState
import frc.junglerobotics3627.keith.subsystem.CraneSubsystem.UpperCraneState
import frc.junglerobotics3627.keith.subsystem.DrivetrainSubsystem
import kotlin.math.pow
import kotlin.math.withSign

object InputSystem {
    init {
        Trigger(::stow).onTrue(StowCommand())

        Trigger(::low).onTrue(DeployCommand().andThen(UpperCommand(UpperCraneState.Low)))
        Trigger(::mid).onTrue(DeployCommand().andThen(UpperCommand(UpperCraneState.Mid)))
        Trigger(::high).onTrue(DeployCommand().andThen(UpperCommand(UpperCraneState.High)))

        Trigger(::resetYaw).onTrue(Commands.runOnce(DrivetrainSubsystem::resetNavXYaw, DrivetrainSubsystem))
        // Trigger(::restClaw).onTrue(ClawCommand(ClawState.Rest).get)
        Trigger(::openClaw).onTrue(ClawCommand(ClawState.Open).get)
        Trigger(::closeCube).onTrue(ClawCommand(ClawState.CloseCube).get)
        Trigger(::closeCone).onTrue(ClawCommand(ClawState.CloseCone).get)

    }

    // TODO: make this more robust.. kind of like last year.. but not as over-engineered
    private var xboxController: XboxController? = XboxController(0)
    private var flightStick: Joystick? = Joystick(1)
    // private var buttonBoard: Joystick? = Joystick(1)

    /**
     * x input from xbox
     */
    private val primaryDriveXComponent: Double
        get() = axisInput(xboxController?.leftY ?: 0.0) * -1

    /**
     * y input from xbox
     */
    private val primaryDriveYComponent: Double
        get() = axisInput(xboxController?.leftX ?: 0.0) * -1

    /* * * * * * * * * * * * * *
     * Crane Setpoint Controls *
     * * * * * * * * * * * * * */
    private val stow: Boolean
        get() = flightStick?.getRawButtonPressed(ButtonBoardPorts.Buttons.stow) ?: false

    private val low: Boolean
        get() = flightStick?.getRawButtonPressed(ButtonBoardPorts.Buttons.low) ?: false

    private val mid: Boolean
        get() = flightStick?.getRawButtonPressed(ButtonBoardPorts.Buttons.mid) ?: false

    private val high: Boolean
        get() = flightStick?.getRawButtonPressed(ButtonBoardPorts.Buttons.high) ?: false



    /* * * * * * * * *
     * Claw Controls *
     * * * * * * * * */
   // private val restClaw: Boolean
     //   get() = (flightStick?.getRawAxis(ButtonBoardPorts.Buttons.restClaw) ?: 0.0) > 0.5

    private val closeCube: Boolean
        get() = flightStick?.getRawButtonPressed(ButtonBoardPorts.Buttons.closeCube) ?: false

    private val closeCone: Boolean
        get() = flightStick?.getRawButtonPressed(ButtonBoardPorts.Buttons.closeCone) ?: false

    private val openClaw: Boolean
        get() = flightStick?.getRawButtonPressed(ButtonBoardPorts.Buttons.openClaw) ?: false

    /* * * * * * * * *
     * Gyro Controls *
     * * * * * * * * */
    private val resetYaw: Boolean
        get() = xboxController?.yButtonPressed ?: false


    private fun deadZone(value: Double) = if (value in -0.1..0.1) 0.0 else value

    private fun sensitivity(value: Double) = value.pow(2).withSign(value) // parabolic curve softens small inputs

    private fun axisInput(value: Double) = sensitivity(deadZone(value))

    /* * * * * * * * * * * *
     * Autonomous Settings *
     * * * * * * * * * * * */
    private fun autoCommand(): SequentialCommandGroup {
        return if (SmartDashboard.getBoolean("Just Taxi", false)) {
            SequentialCommandGroup(
                Commands.waitSeconds(5.0),
                    Commands.runOnce({ DrivetrainSubsystem.rawVelocityDrive(-2.0, 0.0, 0.0) }, DrivetrainSubsystem),
            )
        } else if (SmartDashboard.getBoolean("Just Score", false)) {
            SequentialCommandGroup(
                ClawCommand(ClawState.CloseCube).get,
                DeployCommand().andThen(UpperCommand(UpperCraneState.High)),
                Commands.runOnce({ DrivetrainSubsystem.rawVelocityDrive(3.0, 0.0, 0.1) }, DrivetrainSubsystem),
                Commands.waitSeconds(5.0),
                Commands.runOnce({ DrivetrainSubsystem.stopMotors() }, DrivetrainSubsystem),
                ClawCommand(ClawState.Open).get,
            )
        } else if (SmartDashboard.getBoolean("Score + Taxi", false)) {
            SequentialCommandGroup(
                ClawCommand(ClawState.CloseCube).get.alongWith(
                    DeployCommand().andThen(UpperCommand(UpperCraneState.High)),
                ),
                Commands.runOnce({ DrivetrainSubsystem.rawVelocityDrive(3.0, 0.0, 0.0) }, DrivetrainSubsystem),
                Commands.waitSeconds(1.0),
                Commands.runOnce({ DrivetrainSubsystem.stopMotors() }),
                ClawCommand(ClawState.Open).get,
                Commands.runOnce({ DrivetrainSubsystem.rawVelocityDrive(-0.5, 0.0, 0.0) }, DrivetrainSubsystem)
                    .alongWith(StowCommand()),
                Commands.waitSeconds(1.0),
                Commands.runOnce({ DrivetrainSubsystem.rawVelocityDrive(-4.01, 0.0, 0.0) }, DrivetrainSubsystem),
                Commands.waitSeconds(2.5),
                Commands.runOnce({ DrivetrainSubsystem.stopMotors() }, DrivetrainSubsystem),
            )
        } else {
            SequentialCommandGroup()
        }
    }

    /* * * * * * * * * * * * * * * * *
     * Publicly available input data *
     * * * * * * * * * * * * * * * * */
    val autoCommand: Command
        get() = autoCommand()

    val upperCraneSpeed: Double
        get() = (flightStick?.y ?: 0.0) * -1

    val stowLowerCrane: Boolean
        get() = (flightStick?.x ?: 0.0) <= -0.5

    val deployLowerCrane : Boolean
        get() = (flightStick?.x ?: 0.0) >= 0.5

    val startingAngle: Double
        get() = SmartDashboard.getNumber("Starting Angle", 0.0)

    val driveX: Double
        get() = maxVelocity * clamp(
            primaryDriveXComponent, // + (secondaryDriveXComponent * ButtonBoardPorts.JoystickAxis.driveDiminisher)
            -1.0,
            1.0,
        )

    val driveY: Double
        get() = maxVelocity * clamp(
            primaryDriveYComponent, // + (secondaryDriveYComponent * ButtonBoardPorts.JoystickAxis.driveDiminisher)
            -1.0,
            1.0,
        )

    val driveZ: Double
        get() = maxVelocity * axisInput(xboxController?.rightX ?: 0.0) * -1

    fun rumble(magnitude: Double) {
        xboxController?.setRumble(GenericHID.RumbleType.kBothRumble, magnitude)
    }
}