package frc.junglerobotics3627.keith

import edu.wpi.first.wpilibj.RobotBase

object Main {
    @JvmStatic
    fun main(args: Array<String>) {
        RobotBase.startRobot { Robot() }
    }
}