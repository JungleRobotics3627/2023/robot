package frc.junglerobotics3627.keith.subsystem

import com.kauailabs.navx.frc.AHRS
import com.pathplanner.lib.PathConstraints
import com.pathplanner.lib.PathPlanner
import com.pathplanner.lib.PathPoint
import com.pathplanner.lib.commands.PPMecanumControllerCommand
import com.revrobotics.CANSparkMax
import com.revrobotics.CANSparkMaxLowLevel
import edu.wpi.first.apriltag.AprilTagFields
import edu.wpi.first.math.controller.PIDController
import edu.wpi.first.math.estimator.MecanumDrivePoseEstimator
import edu.wpi.first.math.geometry.*
import edu.wpi.first.math.kinematics.ChassisSpeeds
import edu.wpi.first.math.kinematics.MecanumDriveKinematics
import edu.wpi.first.math.kinematics.MecanumDriveWheelPositions
import edu.wpi.first.math.kinematics.MecanumDriveWheelSpeeds
import edu.wpi.first.wpilibj.smartdashboard.Field2d
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard
import edu.wpi.first.wpilibj2.command.SubsystemBase
import frc.junglerobotics3627.keith.Constants.AprilTagCamera
import frc.junglerobotics3627.keith.Constants.Dimensions.Robot.navXOffsetX
import frc.junglerobotics3627.keith.Constants.Drivetrain
import frc.junglerobotics3627.keith.Constants.Drivetrain.FrontLeft
import frc.junglerobotics3627.keith.Constants.Drivetrain.FrontRight
import frc.junglerobotics3627.keith.Constants.Drivetrain.RearLeft
import frc.junglerobotics3627.keith.Constants.Drivetrain.RearRight
import frc.junglerobotics3627.keith.Constants.Drivetrain.metersPerSecondPerMotorRPM
import frc.junglerobotics3627.keith.ui.InputSystem
import org.photonvision.PhotonCamera
import org.photonvision.PhotonPoseEstimator
import kotlin.math.pow
import kotlin.math.sqrt

object DrivetrainSubsystem : SubsystemBase() {
    private val camera = PhotonCamera(AprilTagCamera.name)

    private val visionPoseEstimator = PhotonPoseEstimator(
        AprilTagFields.k2023ChargedUp.loadAprilTagLayoutField(),
        PhotonPoseEstimator.PoseStrategy.LOWEST_AMBIGUITY, camera,
        Transform3d(Translation3d(AprilTagCamera.x, AprilTagCamera.y, AprilTagCamera.z),
            Rotation3d(AprilTagCamera.roll, AprilTagCamera.pitch, AprilTagCamera.yaw))
    )

    private val minimap = Field2d()

    private val frontLeftMotor = CANSparkMax(FrontLeft.canId, CANSparkMaxLowLevel.MotorType.kBrushless).let {
        it.idleMode = CANSparkMax.IdleMode.kBrake
        it.setSmartCurrentLimit(40)
        it.inverted = FrontLeft.inverted
        it.pidController.p = FrontLeft.p
        it.pidController.i = FrontLeft.i
        it.pidController.d = FrontLeft.d
        it.pidController.iZone = FrontLeft.iZone
        it.pidController.ff = FrontLeft.ff
        it.encoder.positionConversionFactor = Drivetrain.metersPerRotation
        it
    }
    private val frontRightMotor = CANSparkMax(FrontRight.canId, CANSparkMaxLowLevel.MotorType.kBrushless).let {
        it.idleMode = CANSparkMax.IdleMode.kBrake
        it.setSmartCurrentLimit(40)
        it.inverted = FrontRight.inverted
        it.pidController.p = FrontRight.p
        it.pidController.i = FrontRight.i
        it.pidController.d = FrontRight.d
        it.pidController.iZone = FrontRight.iZone
        it.pidController.ff = FrontRight.ff
        it.encoder.positionConversionFactor = Drivetrain.metersPerRotation
        it
    }
    private val rearLeftMotor = CANSparkMax(RearLeft.canId, CANSparkMaxLowLevel.MotorType.kBrushless).let {
        it.idleMode = CANSparkMax.IdleMode.kBrake
        it.setSmartCurrentLimit(40)
        it.inverted = RearLeft.inverted
        it.pidController.p = RearLeft.p
        it.pidController.i = RearLeft.i
        it.pidController.d = RearLeft.d
        it.pidController.iZone = RearLeft.iZone
        it.pidController.ff = RearLeft.ff
        it.encoder.positionConversionFactor = Drivetrain.metersPerRotation
        it
    }
    private val rearRightMotor = CANSparkMax(RearRight.canId, CANSparkMaxLowLevel.MotorType.kBrushless). let {
        it.idleMode = CANSparkMax.IdleMode.kBrake
        it.setSmartCurrentLimit(40)
        it.inverted = RearRight.inverted
        it.pidController.p = RearRight.p
        it.pidController.i = RearRight.i
        it.pidController.d = RearRight.d
        it.pidController.iZone = RearRight.iZone
        it.pidController.ff = RearRight.ff
        it.encoder.positionConversionFactor = Drivetrain.metersPerRotation
        it
    }

    /**
     * Locations of wheels relative to robot center impacts required wheel speeds for desired motions
     */
    private val kinematics = MecanumDriveKinematics(
        Translation2d(FrontLeft.x, FrontLeft.y),
        Translation2d(FrontRight.x, FrontRight.y),
        Translation2d(RearLeft.x, RearLeft.y),
        Translation2d(RearRight.x, RearRight.y),
    )

    private val wheelPositions
        get() = MecanumDriveWheelPositions(
            frontLeftMotor.encoder.position,
            frontRightMotor.encoder.position,
            rearLeftMotor.encoder.position,
            rearRightMotor.encoder.position,
        )

    private val navX = AHRS()

    val pitch
        get() = navX.pitch

    private var currAccel = 0.0
    private var prevAccel = 0.0

    private var velX = 0.0
    private var velY = 0.0
    private var velZ = 0.0

    private val poseEstimator = MecanumDrivePoseEstimator(
        kinematics,
        navX.rotation2d,
        wheelPositions,
        Pose2d(-navXOffsetX, 0.0, Rotation2d())
    )

    private val fieldRelativePose: Pose2d
        get() = poseEstimator.estimatedPosition

    init {
        resetPose()
    }

    override fun periodic() {
        updateFieldPose()
        displayData()
        drive()

        // rumble stuff
        prevAccel = currAccel
        currAccel = sqrt(navX.rawAccelX.pow(2) + navX.rawAccelY.pow(2)).toDouble()
    }

    private fun updateFieldPose() {
        poseEstimator.update(navX.rotation2d, wheelPositions)

        val estimatedRobotPose = visionPoseEstimator.update()
        val visionEstimatedPose = if (estimatedRobotPose.isPresent) estimatedRobotPose.get() else null

        visionEstimatedPose?.let {
            poseEstimator.addVisionMeasurement(it.estimatedPose.toPose2d(), it.timestampSeconds)
        }

        minimap.robotPose = fieldRelativePose
    }

    private fun displayData() {
        SmartDashboard.putString("Field Pose", fieldRelativePose.toString())
        SmartDashboard.putString("Wheel Positions", wheelPositions.toString())
    }

    private fun wheelDrive(wheelSpeeds: MecanumDriveWheelSpeeds) {
        SmartDashboard.putString("Wheel Speeds", wheelSpeeds.toString())

        frontLeftMotor.pidController
                .setReference(wheelSpeeds.frontLeftMetersPerSecond / metersPerSecondPerMotorRPM,
                    CANSparkMax.ControlType.kVelocity)
        frontRightMotor.pidController
                .setReference(wheelSpeeds.frontRightMetersPerSecond / metersPerSecondPerMotorRPM,
                    CANSparkMax.ControlType.kVelocity)
        rearLeftMotor.pidController
                .setReference(wheelSpeeds.rearLeftMetersPerSecond / metersPerSecondPerMotorRPM,
                    CANSparkMax.ControlType.kVelocity)
        rearRightMotor.pidController
                .setReference(wheelSpeeds.rearRightMetersPerSecond / metersPerSecondPerMotorRPM,
                    CANSparkMax.ControlType.kVelocity)
    }

    private fun drive() {
        /*
        val diminisher = if (CraneSubsystem.isUpperCraneHigherThan(CraneSubsystem.UpperCraneState.Mid)) {
            0.5
        } else if (CraneSubsystem.isUpperCraneHigherThan(CraneSubsystem.UpperCraneState.Low)) {
            .75
        } else {
            1.0
        }
        wheelDrive(kinematics.toWheelSpeeds(ChassisSpeeds(velX * diminisher, velY * diminisher, velZ * diminisher)))
         */
        wheelDrive(kinematics.toWheelSpeeds(ChassisSpeeds(velX, velY, velZ)))
    }

    private fun fieldRelativeVelocities(x: Double, y: Double) : Translation2d {
        return Translation2d(x, y).rotateBy(Rotation2d.fromDegrees(navX.yaw.toDouble() + InputSystem.startingAngle))
    }

    fun fieldRelativeDrive(x: Double, y: Double, z: Double) {
        val fieldRelativeVelocities = fieldRelativeVelocities(x, y)

        velX = fieldRelativeVelocities.x
        velY = fieldRelativeVelocities.y
        velZ = z
    }

    fun rawVelocityDrive(x: Double, y: Double, z: Double) {
        velX = x
        velY = y
        velZ = z
    }

    fun getDriveToCommand(destination: Pose2d): PPMecanumControllerCommand { // TODO: use destination pose after tuning PID
        val currentVelocities = fieldRelativeVelocities(navX.velocityX.toDouble(), navX.velocityY.toDouble())
        val trajectory = PathPlanner.generatePath(
                PathConstraints(Drivetrain.velocityLimit, Drivetrain.accelLimit),
                // position, heading(direction of travel), holonomic rotation
                PathPoint.fromCurrentHolonomicState(fieldRelativePose,
                    ChassisSpeeds(currentVelocities.x, currentVelocities.y, 0.0)),
                PathPoint(destination.translation, destination.rotation)
        )

        return PPMecanumControllerCommand(
                trajectory,
                ::fieldRelativePose, // Pose supplier
                kinematics, // MecanumDriveKinematics
                PIDController(0.0, 0.0, 0.0), // X controller. Tune these values for your robot. Leaving them 0 will only use feedforwards.
                PIDController(0.0, 0.0, 0.0), // Y controller (usually the same values as X controller)
                PIDController(0.0, 0.0, 0.0), // Rotation controller. Tune these values for your robot. Leaving them 0 will only use feedforwards.
                3.0, // Max wheel velocity meters per second
                ::wheelDrive,
                false, // Should the path be automatically mirrored depending on alliance color. Optional, defaults to true
                this // Requires this drive subsystem
        )
    }

    fun resetPose() {
        poseEstimator.resetPosition(navX.rotation2d, wheelPositions, fieldRelativePose)
    }

    fun resetNavXYaw() {
        navX.reset()
    }

    fun stopMotors() {
        velX = 0.0
        velY = 0.0
        velZ = 0.0
        frontLeftMotor.stopMotor()
        frontRightMotor.stopMotor()
        rearLeftMotor.stopMotor()
        rearRightMotor.stopMotor()
    }
}