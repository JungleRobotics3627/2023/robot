package frc.junglerobotics3627.keith.subsystem

import com.revrobotics.CANSparkMax
import com.revrobotics.CANSparkMaxLowLevel
import edu.wpi.first.math.MathUtil
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard
import edu.wpi.first.wpilibj2.command.SubsystemBase
import frc.junglerobotics3627.keith.Constants.Crane.Claw
import frc.junglerobotics3627.keith.Constants.Crane.Lower
import frc.junglerobotics3627.keith.Constants.Crane.Upper
import kotlin.math.abs
import kotlin.math.sin

/**
 * Oh, the CraneSubsystem.. how much pain you have caused
 *
 * So, the CraneSubsystem represents the "crane" component on the robot, which has 2 degrees of freedom and a claw at
 * the end. Looks sort of like this.. excuse the bad ASCII drawing
 *
 * ```
 *        /\
 *       /  \
 *      /    <
 * [==========]
 * ```
 *
 * `\` is the "upper crane" which is driven by a sprocket and chain that can raise it to the three required setpoints
 * (low, for ground pickup; mid, for mid nodes; high, for high nodes)
 *
 * `/` is the "lower crane" which is driven by a winch and pulley that can lower it to a deployed state and return it to
 * the stowed state
 *
 * `<` is the "claw" which is driven by a NEO with gear reduction that pinches game pieces. the claw does not have a wrist
 * joint and is parallel with the "upper crane"
 *
 * A physical restraint on this motion that we must consider is that the claw will collide with the chassis if the lower
 * crane is being moved between the stowed and deployed states. (Additionally, the claw cannot be open when moving
 * to/from the stowed position due to the tensioners on the frame, but we will just assume the driver won't stow the
 * crane while carrying cargo, because that would just be silly)
 *
 * The behaviour we want in order to accommodate this:
 * - when stowed or deployed, the upper crane can be in any position
 * - while stowing or deploying, the upper crane must be moved to mid position (or higher) before the lower crane can
 *   begin to move ([isUpperCraneSafeForLowerCraneMotion] will be used to check if the upper crane is high enough)
 *
 * The user interface uses seven buttons as follows:
 * - `stow`: both cranes fully retract and claw is turned off
 * - `low`: lower crane deployed, upper crane in ground position, claw unaffected
 * - `mid`: lower crane deployed, upper crane at mid position, claw unaffected
 * - `high`: lower crane deployed, upper crane at high position, claw unaffected
 * - `openClaw`: opens the claw
 * - `closeCube`: closes the claw with low motor power to not pop the cube game piece
 * - `closeCone`: closes the claw with high motor power to securely compress the cone
 */
object CraneSubsystem : SubsystemBase() {
    /**
     * The base crane arm powered by winch and pulley that has another crane arm attached to its end
     */
    private val lowerCraneMotor = CANSparkMax(Lower.canId, CANSparkMaxLowLevel.MotorType.kBrushless).let {
        it.idleMode = CANSparkMax.IdleMode.kBrake
        it.setSmartCurrentLimit(30) // this is powered by a NEO 550, so I want it even lower

        it.setSoftLimit(CANSparkMax.SoftLimitDirection.kForward, Lower.forwardLimit.toFloat())
        it.setSoftLimit(CANSparkMax.SoftLimitDirection.kReverse, Lower.reverseLimit.toFloat())
        it.enableSoftLimit(CANSparkMax.SoftLimitDirection.kForward, true)
        it.enableSoftLimit(CANSparkMax.SoftLimitDirection.kReverse, true)

        it.closedLoopRampRate = 0.0
        it.openLoopRampRate = 0.0

        it.burnFlash()
        it
    }

    /**
     * The crane arm powered by sprocket and chain and has the claw attached to its end
     */
    private val upperCraneMotor = CANSparkMax(Upper.canId, CANSparkMaxLowLevel.MotorType.kBrushless).let {
        it.idleMode = CANSparkMax.IdleMode.kBrake
        it.setSmartCurrentLimit(40)

        it.setSoftLimit(CANSparkMax.SoftLimitDirection.kForward, Upper.forwardLimit.toFloat())
        it.setSoftLimit(CANSparkMax.SoftLimitDirection.kReverse, Upper.reverseLimit.toFloat())
        it.enableSoftLimit(CANSparkMax.SoftLimitDirection.kForward, true)
        it.enableSoftLimit(CANSparkMax.SoftLimitDirection.kReverse, true)

        it.pidController.p = Upper.PID.p
        it.pidController.i = Upper.PID.i
        it.pidController.d = Upper.PID.d
        it.pidController.iZone = Upper.PID.iZone
        it.pidController.ff = Upper.PID.fSinFactor //initial value, will be updated in periodic

        it.closedLoopRampRate = 0.0
        it.openLoopRampRate = 0.2

        it.burnFlash()
        it
    }

    private val clawMotor = CANSparkMax(Claw.canId, CANSparkMaxLowLevel.MotorType.kBrushless).let {
        it.idleMode = CANSparkMax.IdleMode.kBrake
        it.setSmartCurrentLimit(40)

        it.setSoftLimit(CANSparkMax.SoftLimitDirection.kForward, Claw.forwardLimit.toFloat())
        it.enableSoftLimit(CANSparkMax.SoftLimitDirection.kForward, true)
        //it.setSoftLimit(CANSparkMax.SoftLimitDirection.kReverse, -2.0F)
        it.enableSoftLimit(CANSparkMax.SoftLimitDirection.kReverse, false)

        it.setSoftLimit(CANSparkMax.SoftLimitDirection.kReverse, Claw.reverseLimit.toFloat())
        it.enableSoftLimit(CANSparkMax.SoftLimitDirection.kReverse, true)

        it.closedLoopRampRate = 0.25
        it.openLoopRampRate = 0.25

        it.burnFlash()
        it
    }

    /**
     * The set voltage for the lower crane motor
     */
    private var lowerCraneSetpoint = 0.0

    /**
     * The set position for the upper crane motor
     */
    private var upperCraneSetpoint = 0.0
    private var upperCraneSetSpeed = 0.0
    private var upperCraneUseSpeed = false

    /**
     * The set voltage for the claw motor
     */
    private var clawSetpoint = 0.0

    /**
     * Tells whether the claw is closed enough that motion of the lower crane will not cause self-collision
     */
    val isClawSafeForLowerCraneMotion: Boolean
        get() = clawMotor.encoder.position <= Claw.safeForLowerCraneMotion // less than because claw closes in reverse

    val isClawOpen: Boolean
        get() = clawMotor.encoder.position >= (Claw.forwardLimit - Claw.margin)

    /**
     * Motor voltages must be set periodically in order to maintain the desired motion
     */
    override fun periodic() {
        if (isTuningMode) {
            lowerCraneMotor.set(tuningLowerSetpoint)
            safeSetClaw(tuningClawSetpoint)
            if (isRunningTuning) {
                upperCraneMotor.pidController.setReference(tuningCraneSetpoint, CANSparkMax.ControlType.kPosition)
            } else {
                upperCraneMotor.set(0.0)
            }
        } else {
            lowerCraneMotor.set(lowerCraneSetpoint)
            if (upperCraneUseSpeed) {
                upperCraneMotor.set(upperCraneSetSpeed)
            } else {
                // https://www.desmos.com/calculator/scqewtblgg
                upperCraneMotor.pidController.ff = Upper.PID.fSinFactor *
                        abs(sin((upperCraneMotor.encoder.position + Upper.PID.offset) * Upper.PID.rotationsToRadians))
                upperCraneMotor.pidController.setReference(upperCraneSetpoint, CANSparkMax.ControlType.kPosition)
            }
            safeSetClaw(clawSetpoint)
        }

        SmartDashboard.putNumber("Lower Crane Pos", lowerCraneMotor.encoder.position)
        SmartDashboard.putNumber("Lower Crane Set", lowerCraneSetpoint)
        SmartDashboard.putBoolean("Lower Crane Stowed", checkLowerCranePosition(LowerCraneState.Stow))
        SmartDashboard.putBoolean("Lower Crane Deployed", checkLowerCranePosition(LowerCraneState.Deploy))
        SmartDashboard.putNumber("Upper Crane Pos", upperCraneMotor.encoder.position)
        SmartDashboard.putNumber("Upper Crane Setpoint", upperCraneSetpoint)
        SmartDashboard.putNumber("Upper Crane Set Vel", upperCraneSetSpeed)
        SmartDashboard.putBoolean("Upper Crane Stowed", checkUpperCranePosition(UpperCraneState.Stow))
        SmartDashboard.putBoolean("Upper Crane Safe", checkUpperCranePosition(UpperCraneState.Safe))
        SmartDashboard.putBoolean("Upper Crane Low", checkUpperCranePosition(UpperCraneState.Low))
        SmartDashboard.putBoolean("Upper Crane Mid", checkUpperCranePosition(UpperCraneState.Mid))
        SmartDashboard.putBoolean("Upper Crane High", checkUpperCranePosition(UpperCraneState.High))
        SmartDashboard.putNumber("Claw Set", clawSetpoint)
        SmartDashboard.putNumber("Claw Pos", clawMotor.encoder.position)
        SmartDashboard.putBoolean("Is Claw Safe", isClawSafeForLowerCraneMotion)
    }

    /**
     * Safely sets the claw, preventing it from being open when behind the tensioners
     */
    private fun safeSetClaw(speed: Double) {
        // only an issue if the lower crane is closed and the upper crane is *lower* than -8 (checkUpperCrane(Safe)
        // would not react quickly enough
        if (checkLowerCranePosition(LowerCraneState.Stow) && !isUpperCraneHigherThan(UpperCraneState.Safe)) {
            // if it's already set to close, we want to keep that voltage so that we don't drop a game piece
            if (speed < Claw.closePassiveVoltage) {
                clawMotor.set(speed)
            } else {
                clawMotor.set(Claw.closePassiveVoltage)
            }
        } else {
            clawMotor.set(speed)
        }
    }

    fun setLowerCrane(setpoint: LowerCraneState) {
        lowerCraneSetpoint = when (setpoint) {
            LowerCraneState.Rest -> 0.0
            LowerCraneState.Stow -> Lower.stowVoltage
            LowerCraneState.Deploy -> Lower.deployVoltage
        }
    }

    fun setUpperCraneSetpoint(setpoint: UpperCraneState) {
        upperCraneUseSpeed = false
        upperCraneSetpoint = when (setpoint) {
            UpperCraneState.Stow -> Upper.stowSetpoint
            UpperCraneState.Safe -> Upper.safeSetpoint
            UpperCraneState.Low -> Upper.lowSetpoint
            UpperCraneState.Mid -> Upper.midSetpoint
            UpperCraneState.High -> Upper.highSetpoint
        }
    }

    fun setUpperCraneRaw(speed: Double) {
        upperCraneUseSpeed = true
        upperCraneSetSpeed = speed * .5
    }

    fun setClaw(setpoint: ClawState) {
        clawSetpoint = when (setpoint) {
            ClawState.Rest -> 0.0
            ClawState.Open -> Claw.openVoltage
            ClawState.CloseCube -> Claw.closeCubeVoltage
            ClawState.CloseCone -> Claw.closeConeVoltage
        }
    }

    fun adjustUpperCraneSetpoint(adjustment: Double) {
        upperCraneSetpoint = MathUtil.clamp(upperCraneSetpoint + adjustment, Upper.reverseLimit, Upper.forwardLimit)
    }

    /**
     * Verifies the lower crane has reached the [setpoint]
     *
     * @throws IllegalArgumentException when [setpoint] is [LowerCraneState.Rest]
     */
    fun checkLowerCranePosition(setpoint: LowerCraneState): Boolean {
        return when (setpoint) {
            LowerCraneState.Rest -> throw IllegalArgumentException("LowerCrane rest position cannot be checked")
            LowerCraneState.Stow -> lowerCraneMotor.encoder.position <= (Lower.reverseLimit + Lower.margin)
            LowerCraneState.Deploy -> lowerCraneMotor.encoder.position >= (Lower.forwardLimit - Lower.margin)
        }
    }

    /**
     * Verifies the upper crane has reached the [setpoint]
     */
    fun checkUpperCranePosition(setpoint: UpperCraneState): Boolean {
        return when (setpoint) {
            UpperCraneState.Stow -> upperCraneMotor.encoder.position >= Upper.stowSetpoint - Upper.margin
            // can move to Safe position without worrying about lower crane position
            UpperCraneState.Low -> {
                upperCraneMotor.encoder.position in
                        (Upper.lowSetpoint - Upper.margin)..(Upper.lowSetpoint + Upper.margin)
            }
            UpperCraneState.Safe -> upperCraneMotor.encoder.position <= Upper.safeForLowerCraneMotion
            UpperCraneState.Mid -> {
                upperCraneMotor.encoder.position in
                        (Upper.midSetpoint - Upper.margin)..(Upper.midSetpoint + Upper.margin)
            }

            UpperCraneState.High -> upperCraneMotor.encoder.position <= Upper.highSetpoint + Upper.margin
        }
    }

    fun isUpperCraneHigherThan(height: UpperCraneState): Boolean {
        return when (height) {
            UpperCraneState.Stow -> upperCraneMotor.encoder.position <= Upper.stowSetpoint
            UpperCraneState.Low -> upperCraneMotor.encoder.position <= Upper.lowSetpoint
            UpperCraneState.Safe -> upperCraneMotor.encoder.position <= Upper.safeSetpoint
            UpperCraneState.Mid -> upperCraneMotor.encoder.position <= Upper.midSetpoint
            UpperCraneState.High -> upperCraneMotor.encoder.position <= Upper.highSetpoint
        }
    }

    enum class LowerCraneState { Rest, Stow, Deploy, }
    enum class UpperCraneState { Stow, Low, Safe, Mid, High, }
    enum class ClawState { Rest, Open, CloseCube, CloseCone, }

    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * The following attributes and methods are strictly created for the purpose of the TuneCraneCommand *
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
    var isRunningTuning = false
    var isTuningMode = false
    var tuningCraneSetpoint = 0.0
    val tuningCraneProcessVariable
        get() = upperCraneMotor.encoder.position
    var tuningLowerSetpoint = 0.0
    val tuningLowerProcessVariable
        get() = lowerCraneMotor.encoder.position
    var tuningClawSetpoint = 0.0
    val tuningClawProcessVariable
        get() = clawMotor.encoder.position

    fun tuneSetPIDF(p: Double, i: Double, d: Double, f: Double) {
        upperCraneMotor.pidController.p = p
        upperCraneMotor.pidController.i = i
        upperCraneMotor.pidController.d = d
        upperCraneMotor.pidController.ff = f
    }
}
