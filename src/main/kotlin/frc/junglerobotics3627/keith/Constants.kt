package frc.junglerobotics3627.keith

import edu.wpi.first.math.geometry.Translation2d
import edu.wpi.first.math.util.Units.inchesToMeters
import kotlin.math.PI

object Constants {
    object Drivetrain {

        const val velocityLimit = 4.0 // m/s
        const val accelLimit = 3.0 // m/s

        /**
         * The conversion factor for motor shaft rpm to meters per second traversed by the wheel
         *
         * (wheel diameter (inches) * .0254 (meters/inch conversion) * π) / (gear ratio * 60 (seconds/minute conversion)
         */
        const val metersPerSecondPerMotorRPM = (6 * .0254 * PI) / (7.31 * 60)

        /**
         * The conversion factor for motor shaft rotations to meters traversed by the wheel
         *
         * wheel diameter (inches) * .0254 (meters/inch conversion) * π / gear ratio
         */
        const val metersPerRotation = 6 * .0254 * PI / 7.31

        const val maxVelocity = 6000 * metersPerSecondPerMotorRPM // max motor rpm (with weight of robot) * conversion factor

        object FrontLeft {
            const val x = 10.095 * .0254 // inches to meters
            const val y = 11.49 * .0254

            const val canId = 14
            const val inverted = false

            const val p = 0.00015
            const val i = 0.000001
            const val d = 0.00002
            const val iZone = 5.0
            const val ff = 0.000014
        }

        object FrontRight {
            const val x = 10.095 * .0254
            const val y = -11.49 * .0254

            const val canId = 11
            const val inverted = true

            const val p = 0.00015
            const val i = 0.000001
            const val d = 0.00002
            const val iZone = 5.0
            const val ff = 0.000014
        }

        object RearLeft {
            const val x = -10.095 * .0254
            const val y = 11.49 * .0254

            const val canId = 13
            const val inverted = false

            const val p = 0.00015
            const val i = 0.000001
            const val d = 0.00002
            const val iZone = 5.0
            const val ff = 0.000014
        }

        object RearRight {
            const val x = -10.095 * .0254
            const val y = -11.49 * .0254

            const val canId = 12
            const val inverted = true

            const val p = 0.00015
            const val i = 0.000001
            const val d = 0.00002
            const val iZone = 5.0
            const val ff = 0.000014
        }

        object Balancing {
            const val p = 0.0
            const val i = 0.0
            const val d = 0.0
            const val iZone = 5.0
        }
    }

    object AprilTagCamera {
        const val name = "placeholder"
        const val x = 0.04676 //meters
        const val y = 0.23495 //meters
        const val z = 0.0
        const val roll = 90.0
        const val pitch = 0.0
        const val yaw = 0.0 // TODO possibly nonzero
    }

    object ClawCamera { //TODO maybe not putting on claw
        const val name = "claw_cam"
    }

    object Crane {
        object Upper {
            const val canId = 22

            const val stowSetpoint = 0.0 // good value I think, should technically be 0.238095 but close enough
            const val lowSetpoint = -5.0 // good value
            const val safeSetpoint = -8.0 // good value I think
            const val midSetpoint = -28.0 // approximate value
            const val highSetpoint = -36.0 // good value

            const val safeForLowerCraneMotion = -4.0 // is safe here; claw seems to be more important in this matter
            const val margin = 0.5 // margin around setpoints in motor rotations

            const val velocity = 1.0
            const val acceleration = 0.5

            const val forwardLimit = 1.0 // stowed
            const val reverseLimit = -41.0 // deployed

            object PID {
                const val p = 0.0065
                const val i = 0.00001
                const val d = 0.00001
                const val iZone = 5.0
                const val fSinFactor = 0.00475 // constant multiplier of sin(armAngle) that results in angle-specific ff

                const val rotationsToRadians = 2 * PI / 102.4 // radians per motor rotations
                const val offset = -3.93 // encoder position for vertical upper arm
            }
        }

        object Lower {
            const val canId = 21

            const val forwardLimit = 140.5 // deployed
            const val reverseLimit = 0.0 // stowed

            const val margin = 10.0 // margin around setpoints in motor rotations

            const val stowVoltage = -0.4
            const val deployVoltage = 0.2
        }

        object Claw {
            const val canId = 31

            /**
             * Claw should not open past this point, would put stress on claw mechanism
             */
            const val forwardLimit = 4.5

            /**
             * Claw should not close past this point, torques claw and skips gearbox gears
             */
            const val reverseLimit = 1.2 //TODO find actual value

            const val margin = 0.5 // margin around setpoints in motor rotations

            /**
             * Claw has to be closed to this point for it to not hit tensioners and wires
             */
            const val safeForLowerCraneMotion = 2.14

            const val openVoltage = 0.3
            const val closePassiveVoltage = -0.15
            const val closeCubeVoltage = -0.25
            const val closeConeVoltage = -0.75
        }
    }

    object Dimensions {
        object Robot {
            const val depth = 38.31 * 0.0254
            const val width = 33.00 * 0.0254
            const val navXOffsetX = 0.248
        }

        object Field {
            const val fieldDepth = 651.22 * 0.0254

            object Grids {
                const val fullAssemblyDepth = 56.25 * 0.0254
                const val depthBetweenNodes = 20.808 * 0.0254
                const val lengthBetweenNodes = 22.0 * 0.0254

                val nodeScoringLocations = mapOf(
                    // blue alliance wall
                    1 to Translation2d(fullAssemblyDepth + Robot.depth /2, inchesToMeters(20.19 + 22 * 8)),
                    2 to Translation2d(fullAssemblyDepth + Robot.depth /2, inchesToMeters(20.19 + 22 * 7)),
                    3 to Translation2d(fullAssemblyDepth + Robot.depth /2, inchesToMeters(20.19 + 22 * 6)),
                    4 to Translation2d(fullAssemblyDepth + Robot.depth /2, inchesToMeters(20.19 + 22 * 5)),
                    5 to Translation2d(fullAssemblyDepth + Robot.depth /2, inchesToMeters(20.19 + 22 * 4)),
                    6 to Translation2d(fullAssemblyDepth + Robot.depth /2, inchesToMeters(20.19 + 22 * 3)),
                    7 to Translation2d(fullAssemblyDepth + Robot.depth /2, inchesToMeters(20.19 + 22 * 2)),
                    8 to Translation2d(fullAssemblyDepth + Robot.depth /2, inchesToMeters(20.19 + 22)),
                    9 to Translation2d(fullAssemblyDepth + Robot.depth /2, inchesToMeters(20.19)),
                    // red alliance wall
                    11 to Translation2d(fieldDepth - fullAssemblyDepth - Robot.depth /2, inchesToMeters(20.19)),
                    12 to Translation2d(fieldDepth - fullAssemblyDepth - Robot.depth /2, inchesToMeters(20.19 + 22)),
                    13 to Translation2d(fieldDepth - fullAssemblyDepth - Robot.depth /2, inchesToMeters(20.19 + 22 * 2)),
                    14 to Translation2d(fieldDepth - fullAssemblyDepth - Robot.depth /2, inchesToMeters(20.19 + 22 * 3)),
                    15 to Translation2d(fieldDepth - fullAssemblyDepth - Robot.depth /2, inchesToMeters(20.19 + 22 * 4)),
                    16 to Translation2d(fieldDepth - fullAssemblyDepth - Robot.depth /2, inchesToMeters(20.19 + 22 * 5)),
                    17 to Translation2d(fieldDepth - fullAssemblyDepth - Robot.depth /2, inchesToMeters(20.19 + 22 * 6)),
                    18 to Translation2d(fieldDepth - fullAssemblyDepth - Robot.depth /2, inchesToMeters(20.19 + 22 * 7)),
                    19 to Translation2d(fieldDepth - fullAssemblyDepth - Robot.depth /2, inchesToMeters(20.19 + 22 * 8)),
                )
            }

            object Community {
                const val shortDepth = 132.375 * 0.0254
                const val longDepth = 193.25 * 0.0254
                const val shortWidth = 59.39 * 0.0254 + ChargeStation.width
            }

            object ChargeStation {
                const val width = 97.25 * 0.0254
                const val levelAngle = 2.5 //degrees; when within +/- this angle, CS is assumed to be level
                const val boardedAngle = 8 //degrees; when >= this angle, robot is assumed to have boarded CS; maybe change value
            }

            val stagedCargoLocations = mapOf(
                // blue alliance half
                "A" to Translation2d(Grids.fullAssemblyDepth + inchesToMeters(224.00), inchesToMeters(36.19 + 48.00 * 3)),
                "B" to Translation2d(Grids.fullAssemblyDepth + inchesToMeters(224.00), inchesToMeters(36.19 + 48.00 * 2)),
                "C" to Translation2d(Grids.fullAssemblyDepth + inchesToMeters(224.00), inchesToMeters(36.19 + 48.00)),
                "D" to Translation2d(Grids.fullAssemblyDepth + inchesToMeters(224.00), inchesToMeters(36.19)),
                // red alliance half
                "E" to Translation2d(Grids.fullAssemblyDepth + inchesToMeters(224.00 + 47.36 * 2), inchesToMeters(36.19)),
                "F" to Translation2d(Grids.fullAssemblyDepth + inchesToMeters(224.00 + 47.36 * 2), inchesToMeters(36.19 + 48.00)),
                "G" to Translation2d(Grids.fullAssemblyDepth + inchesToMeters(224.00 + 47.36 * 2), inchesToMeters(36.19 + 48.00 * 2)),
                "H" to Translation2d(Grids.fullAssemblyDepth + inchesToMeters(224.00 + 47.36 * 2), inchesToMeters(36.19 + 48.00 * 3))
            )
        }
    }

    object ButtonBoardPorts {
        object JoystickAxis {
            const val driveDiminisher = 0.0694200
            const val x = 0
            const val y = 1
        }

        object Buttons {
            const val stow = 6
            const val low = 7
            const val mid = 10
            const val high = 11
            const val stowLowerCrane = 2
            const val deployLowerCrane = 3

            //const val restClaw = 4 // axis port number
            const val openClaw = 4 // axis port number
            const val closeCube = 1
            const val closeCone = 5

            const val resetPose = 9
        }
    }

    /*
    object LogitechAttack3 {
        const val flightStickDriveDiminisher = 0.0694200

        object CommandButtons {
            const val score = 3
        }

        object CraneButtons {
            const val stow = 9
            const val setLow = 8
            const val setMid = 7
            const val setHigh = 6
        }

        object ClawButtons {
            const val open = 2
            const val closeCube = 4
            const val closeCone = 5
        }
    }
    */
}

